#target photoshop

var docRef = app.activeDocument;

docRef.pathItems.add("Hexagons", makeHexaNetOfHexagons(0, 0, 800, 800, 80, 60, 50, 30, 20));

function makeHexaNetOfHexagons(startX,
                               startY,
                               width,
                               height,
                               outerRadiusUp,
                               innerRadiusUp,
                               outerRadiusDown,
                               innerRadiusDown,
                               distance) {

    var hexaNetRadius = outerRadiusUp + distance / 2.0;
    var hexaNet = makeHexaNet(startX, startY, width, height, hexaNetRadius);
    var hexagons = new Array();
    var hexaNetRadiusY = hexaNetRadius * 0.86602540378
    
    for (var i = 0; i != hexaNet.length; i++) {
        var x = hexaNet[i][0]
        var y = hexaNet[i][1]
        
        var t = ((y - startY - hexaNetRadiusY) / (height - 2 * hexaNetRadiusY)) 
                * Math.pow((x - startX - hexaNetRadius) / (width - 2 * hexaNetRadius), 0.5);
        
        var hexagon = makeHexagon(x, y, 
                                  outerRadiusUp - ((outerRadiusUp - outerRadiusDown) * t),
                                  innerRadiusUp - ((innerRadiusUp - innerRadiusDown) * t));
        hexagons = hexagons.concat(hexagon);
    }

    return hexagons;
}

function makeHexaNet(startX, startY, width, height, radius) {
    var radiusY = radius * 0.86602540378
    
    var countX = Math.floor((width - 2 * radius) / (1.5 * radius)) + 1;
    var countY = Math.floor((height - 2 * radiusY) / (radiusY)) + 1;
    
    var net = new Array();
    
    for (var i = 0; i != countX; i++) {
        for (var j = 0; j != countY; j++) {
            if ((i % 2 == 1 && j % 2 == 0) || (i % 2 == 0 && j % 2 == 1)) {
                net.push(new Array(startX + (1 + i * 1.5) * radius, startY + (1 + j) * radiusY));
            }
        }
    }

    return net;
}

function makeHexagon(centerX, centerY, outerRadius, innerRadius) {
    var hexagonPathArray = new Array();
    
    var outerHexagon = makeSingleHexagon(centerX, centerY, outerRadius);
    var innerHexagon = makeSingleHexagon(centerX, centerY, innerRadius);
    
    outerHexagon.operation = ShapeOperation.SHAPEADD;
    innerHexagon.operation = ShapeOperation.SHAPESUBTRACT;
    
    hexagonPathArray.push(outerHexagon);
    hexagonPathArray.push(innerHexagon);
    
    return hexagonPathArray;
}

function makeSingleHexagon(centerX, centerY, radius) {
    var lineArray = new Array();
    
    for (var i = 0; i != 6; i++) {
        var pathPoint = new PathPointInfo()
        var angle = i * Math.PI / 3.0
        var point = new Array(centerX + radius * Math.cos (angle), centerY + radius * Math.sin(angle))
        
        pathPoint.kind = PointKind.CORNERPOINT;
        pathPoint.anchor = point;
        pathPoint.leftDirection = point;
        pathPoint.rightDirection = point;
                
        lineArray.push(pathPoint);
    }

    var pathInfo = new SubPathInfo();
    pathInfo.closed = true;
    pathInfo.entireSubPath = lineArray;
    
    return pathInfo;
}