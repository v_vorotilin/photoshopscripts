﻿var folderPath = "G:\\Projects\\omEga\\icons\\66";
var outputFolderPath = "G:\\Projects\\omEga\\icons\\66\\output";
var sizes = new Array({ w: 22, h: 22 }, { w: 44, h: 44 }, { w: 66, h: 66 });

var folder = new Folder(folderPath);

if (folder != null) {
    var files = folder.getFiles(/\.(png|)$/i);
    
    for (var i = 0; i != files.length; i++) {
        var doc = open(files[i]);
        
        for (var j = 0; j != sizes.length; j++) {
            resize(doc, sizes[j].w, sizes[j].h);
            exportDocument(doc, getNameForSize(doc, sizes[j]))
        }
        
        doc.close(SaveOptions.DONOTSAVECHANGES);
    }
}

function resize(doc, width, height) {
    // do the resizing.  if height > width (portrait-mode) resize based on height.  otherwise, resize based on width
    if (doc.height > doc.width) {
        doc.resizeImage(null, UnitValue(height, "px"), null, ResampleMethod.BICUBIC);
    } else {
        doc.resizeImage(UnitValue(width, "px"), null, null, ResampleMethod.BICUBIC);
    }

    // Convert the canvas size as informed above for the END RESULT
    doc.resizeCanvas(UnitValue(width, "px"), UnitValue(height,"px"));
}

function getNameForSize(doc, size) {
    return outputFolderPath + "\\" + doc.name + size.w + "x" + size.h + ".png";
}

function exportDocument(doc, filename) {
    var options = new ExportOptionsSaveForWeb();
    options.format = SaveDocumentType.PNG;
    options.PNG8 = false;
    options.quality = 100;
    
    doc.exportDocument(new File(filename), ExportType.SAVEFORWEB, options);
}